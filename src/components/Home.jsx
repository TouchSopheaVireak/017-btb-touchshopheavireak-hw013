import React, { Component } from 'react'
import {Nav,Navbar,FormControl,Form,Button,Table, Container} from 'react-bootstrap';
import {Link} from 'react-router-dom';
import 'moment-timezone';
import Moment from 'react-moment';
import axios from 'axios';
import View from './View';

export default class Home extends Component {
    state = {
        info: [],
    };
    async componentWillMount(){
        axios.get('http://110.74.194.125:3535/api/articles')
            .then(res => {
                // console.log(res.data);
                this.setState({ info: res.data.data });
            })
            .catch(function (error) {
                // console.log(error);
            });
    }
    
    render() {
        return (
            <div>
                <div className="text-center">
                    <h1 className="text-center">Article Management</h1>
                    <Button variant="outline-info" >
                        <Link to="/add">Add Article</Link>
                    </Button>
                </div>

                <br/>
                <Container>
                    <div className="container-fliud">
                        <Table striped bordered hover>
                            <thead>
                                <tr>
                                <th>#</th>
                                <th>TITLE</th>
                                <th>DESCRIPTION</th>
                                <th>CREATE DATE</th>
                                <th>IMAGE</th>
                                <th>ACTION</th>
                                </tr>
                            </thead>
                            <tbody>
                                {this.state.info.map((infos) => (
                                    <tr key={infos._id}>
                                        <td>{infos._id}</td>
                                        <td>{infos.title}</td>
                                        <td className="w-50 p-3">{infos.description}</td>
                                        <td>
                                            <Moment format="YYYY-MM-DD">{infos.createdAt.localtime}</Moment>
                                        </td>
                                        <td><img src={infos.image} width="120" height="120" alt=""/></td>
                                        <td>
                                            <Button variant="outline-info">
                                                <Link to={{pathname: '/View',params:{id: infos._id}}}>View</Link>
                                                {/* <Link to="/View" params={{ id: infos._id }}>View</Link> */}
                                            </Button>
                                            <Button variant="outline-info">
                                                <Link to="/add">Edit</Link>
                                            </Button>
                                            <Button variant="outline-info">
                                                <Link to="/add">Delete</Link>
                                            </Button>
                                        </td>
                                    </tr>
                                ))}
                            </tbody>
                        </Table>
                    </div>
                </Container>

            </div>
        );
    }
}
