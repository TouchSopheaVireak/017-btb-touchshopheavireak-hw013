import React, { Component } from 'react'
import {Nav,Navbar,FormControl,Form,Button,Table, Container} from 'react-bootstrap';
import {Link} from 'react-router-dom';

export default class Menu extends Component {
    render() {
        return (
            <div>
                 <Navbar bg="dark" variant="dark">
                    <Container>  
                        <Navbar.Brand>AMS</Navbar.Brand>  
                        <Nav className="mr-auto">
                            <Nav.Link as = {Link} to ="/home">Home</Nav.Link>
                        </Nav>
                        <Form inline>
                            <FormControl type="text" placeholder="Search" className="mr-sm-2" />
                            <Button variant="outline-info">
                                <Link to="/Search">Search</Link>
                            </Button>
                        </Form>     
                    </Container>
                </Navbar>
            </div>
        )
    }
}
