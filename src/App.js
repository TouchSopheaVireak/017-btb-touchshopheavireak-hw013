import React from 'react';
import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import {BrowserRouter as Rotuer, Switch, Route} from 'react-router-dom'
import Home from './components/Home';
import Add from './components/Add';
import Search from './components/Search';
import View from './components/View';
import Menu from './components/Menu';

function App() {
  return (
    <div className="App">
      
      <Rotuer>
      <Menu />
        <Switch>
          <Route path = "/" exact component = {Home} />
          <Route path = "/Home" component = {Home} />
          <Route path = "/Add" component = {Add} />
          <Route path =  "/Search" component = {Search} />
          <Route path = "/View" component = {View} />
        </Switch>
      </Rotuer>
    </div>
  );
}

export default App;
